# ZDROJE:
# https://www.geeksforgeeks.org/flask-creating-first-simple-application/
# https://jdhao.github.io/2020/04/12/build_webapi_with_flask_s2/
# https://pythonbasics.org/flask-upload-file/
# https://gist.github.com/kylehounslow/767fb72fde2ebdd010a0bf4242371594

from io import BytesIO

from PIL import Image
from flask import Flask, request
from flask import send_file
import cv2 as cv
from skimage import io
from skimage.transform import rotate
from skimage.util import img_as_ubyte

app = Flask(__name__)


def serve_pil_image(pil_img, filename_extension, mimetype):
    img_io = BytesIO()
    pil_img.save(img_io, filename_extension, quality=70)
    img_io.seek(0)
    return send_file(img_io, mimetype=mimetype)


@app.route("/im_size", methods=["POST"])
def process_image():
    file = request.files['image']

    img_pil = Image.open(file.stream)
    img_format = img_pil.format
    file_mimetype = file.mimetype

    img_pil = img_pil.transpose(Image.Transpose.ROTATE_90)
    img_pil.save('pil.jpg')

    img_cv = cv.imread('pil.jpg')
    img_cv = cv.rotate(img_cv, cv.ROTATE_90_COUNTERCLOCKWISE)
    cv.imwrite('cv.jpg', img_cv)

    img_scikit = io.imread('cv.jpg')
    img_scikit = rotate(img_scikit, 90, resize=True)
    img_scikit = img_as_ubyte(img_scikit)
    io.imsave('scikit.jpg', img_scikit)

    img = Image.open('scikit.jpg')

    return serve_pil_image(
        img,
        img_format,
        file_mimetype
    )


if __name__ == "__main__":
    app.run(debug=True)
