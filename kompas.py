import imageio
from skimage import io
from skimage.transform import rotate
from skimage.util import img_as_ubyte
from typing import Final
import os

SAMPLE_IMAGE_FILENAME: Final = 'best.png'
MOVING_IMAGE_FILENAME: Final = 'kompas.gif'

img: Final = io.imread(SAMPLE_IMAGE_FILENAME)
SPEED: Final[int] = 9
DEGREES: Final[int] = 360 // SPEED

with imageio.get_writer(MOVING_IMAGE_FILENAME, mode='I') as writer:
    for degree in range(DEGREES):
        print(degree)
        img_rotated = rotate(img, (-degree * SPEED), resize=False)
        img_rotated = img_as_ubyte(img_rotated)
        writer.append_data(img_rotated)

os.system('eog ' + MOVING_IMAGE_FILENAME)
