import os
import webbrowser

import requests

url = 'http://127.0.0.1:5000/im_size'
my_img = {'image': open('arrow.jpg', 'rb')}
r = requests.post(url, files=my_img)

with open("response.jpg", "wb") as text_file:
    n = text_file.write(r.content)

webbrowser.open('file://' + os.path.realpath('response.jpg'))
